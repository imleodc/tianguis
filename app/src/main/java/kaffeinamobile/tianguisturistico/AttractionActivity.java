package kaffeinamobile.tianguisturistico;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kaffeinamobile.tianguisturistico.models.Attraction;


public class AttractionActivity extends BaseActivity {

    private Attraction attraction;

    private boolean isValid(String text){
        return (text!=null && !text.isEmpty() && !text.equals("null"));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction);

        setToolbar();
        activateUpButton();

        Intent intent = getIntent();

        attraction = (Attraction) intent.getSerializableExtra("attraction");

        ((TextView)findViewById(R.id.attraction_name)).setText(attraction.getName());
        ((TextView)findViewById(R.id.attraction_description)).setText(attraction.getDescription());

        ImageView imageview = (ImageView) findViewById(R.id.attraction_picture);
        ImageView thumbnail = (ImageView) findViewById(R.id.category_thumbnail);

        Picasso.with(getApplicationContext()).load(attraction.getSmallIcon()).fit().into(thumbnail);
        int backgroundImage = getResources().getIdentifier("imagen"+attraction.getId(), "drawable", getApplicationContext().getPackageName());


        try{
            if(backgroundImage!=0){
                Picasso.with(getApplicationContext()).load(backgroundImage).into(imageview);
            }else{
                Picasso.with(getApplicationContext()).load(attraction.getParentPicture()).into(imageview);
            }
        }catch (Exception ignored){}

        if(isValid(attraction.getUrl())) {
            ((TextView)findViewById(R.id.attraction_url)).setText(attraction.getUrl());
        }else{
            findViewById(R.id.attraction_url).setVisibility(View.GONE);
        }

        if(isValid(attraction.getContact())) {
            ((TextView)findViewById(R.id.attraction_tel)).setText(attraction.getContact());
        }else{
            findViewById(R.id.attraction_tel).setVisibility(View.GONE);
        }
    }

    public void goToUrl(View view) {
        String contactUrl = attraction.getUrl();
        String url = (!contactUrl.startsWith("http://") && !contactUrl.startsWith("https://")) ? "http://" + contactUrl:contactUrl;

        Intent newIntent = new Intent(Intent.ACTION_VIEW);

        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.setData(Uri.parse(url));

        getApplicationContext().startActivity(newIntent);
    }

    public void requestCall(View view) {

        String data = attraction.getContact();

        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(data);

        Intent newIntent;
        if (m.matches()) {
            newIntent = new Intent(Intent.ACTION_SENDTO);
            newIntent.setType("text/html");
            newIntent.putExtra(Intent.EXTRA_EMAIL, data);
            newIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
            //newIntent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

            startActivity(Intent.createChooser(newIntent, "Send Email"));
        }else{
            newIntent = new Intent(Intent.ACTION_DIAL);

            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            newIntent.setData(Uri.parse("tel:" + attraction.getContact()));

            getApplicationContext().startActivity(newIntent);
        }


    }
}
