package kaffeinamobile.tianguisturistico;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


public class MapasActivity extends BaseActivity {

    //private String map_url = "http://movil-projects.com/tt2017/map";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        setToolbar();

        InputStream input;
        try {
            input = getAssets().open("map.html");
            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            String text = new String(buffer);
            WebView mapView = (WebView) findViewById(R.id.webview_map);

            WebSettings settings = mapView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setDefaultTextEncodingName("UTF-8");

            mapView.loadDataWithBaseURL(null, text, "text/html", "UTF-8", null);

            MyWebViewClient webClient = new MyWebViewClient();
            mapView.setWebViewClient(webClient);

        }catch (Exception ignored){}

    }


    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            /*
            if (Uri.parse(url).getHost().equals(map_url)) {
                return false;
            }
            */
            Intent intent = new Intent(getApplicationContext(), DirectoryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

            String[] split = url.split("name=");

            try {
                intent.putExtra("show_group", URLDecoder.decode(split[1], "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            startActivity(intent);
            return true;
        }
    }

}
