package kaffeinamobile.tianguisturistico;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

import kaffeinamobile.tianguisturistico.models.Contact;
import kaffeinamobile.tianguisturistico.net.HttpGetRequest;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgendaLoginActivity extends BaseActivity {

    private EditText userInput;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_login);

        setToolbar();

        userInput = (EditText) findViewById(R.id.user_id);

        SharedPreferences settings = getApplicationContext().getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);
        String delegate_id = settings.getString("delegate_id", "");

        userInput.setText(delegate_id);
    }

    public void ingresar(final View view) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", getString(R.string.loading), true, false);
        ringProgressDialog.show();

        final String delegate_id = userInput.getText().toString();
        ServiceTianguis serviceTianguis = new ServiceTiaguisImp();

        serviceTianguis.getAgenda(delegate_id, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ringProgressDialog.dismiss();
                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        JSONObject json = new JSONObject(result);

                        if(json.getBoolean("status")){
                            SharedPreferences settings = getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, MODE_PRIVATE);
                            SharedPreferences.Editor edit = settings.edit();
                            edit.putString("delegate_id", delegate_id);
                            edit.apply();

                            Intent intent = new Intent(getApplicationContext(), AgendaActivity.class);

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("user_data", result);

                            getApplicationContext().startActivity(intent);
                        }else {
                            String errors = json.getString("errors");
                            Toast.makeText(getApplicationContext(), errors, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getString(R.string.error_login), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ringProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.error_login), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
