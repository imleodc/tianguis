package kaffeinamobile.tianguisturistico;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import kaffeinamobile.tianguisturistico.adapters.ScreenSlidePageFragment;
import kaffeinamobile.tianguisturistico.models.Contact;
import kaffeinamobile.tianguisturistico.views.CustomPager;

public class RutasActivity extends BaseActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    private MarkerOptions centroid;

    private final ArrayList<MarkerOptions> route_1 = new ArrayList<>();
    private final ArrayList<MarkerOptions> route_2 = new ArrayList<>();
    private final ArrayList<MarkerOptions> route_3 = new ArrayList<>();
    private final ArrayList<MarkerOptions> route_4 = new ArrayList<>();
    private final ArrayList<MarkerOptions> route_5 = new ArrayList<>();

    private PolylineOptions polyline_r1;
    private PolylineOptions polyline_r2;
    private PolylineOptions polyline_r3;
    private PolylineOptions polyline_r4;
    private PolylineOptions polyline_r5;

    private ArrayList<TextView> buttons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rutas);

        setToolbar();
        readData();

        ViewPager pager = (ViewPager) this.findViewById(R.id.routes_pager);

        MyFragmentPagerAdapter pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());

        pagerAdapter.addFragment(ScreenSlidePageFragment.newInstance("DOMINGO 26 MARZO DEL 2017",
                "09:00 A 20:00 hrs", "AEROPUERTO - HOTELES",
                "09:00 A 20:00 hrs", "AEROPUERTO - MUNDO IMPERIAL"));

        pagerAdapter.addFragment(ScreenSlidePageFragment.newInstance("LUNES 27 MARZO DEL 2017",
                "08:00 A 10:00 hrs", "HOTELES - MUNDO IMPERIAL",
                "17:30 A 19:00 hrs", "EXPO MUNDO IMPERIAL - HOTELES",
                "18:00 A 19:30 hrs", "EXPO MUNDO IMPERIAL - HOTEL PRINCESS",
                "23:00 hrs", "COCTEL DE INAUGURACIÓN - HOTELES"));

        pagerAdapter.addFragment(ScreenSlidePageFragment.newInstance("MARTES 28 MARZO DEL 2017",
                "08:00 A 10:00 hrs", "HOTELES - MUNDO IMPERIAL",
                "17:30 A 19:00 hrs", "MUNDO IMPERIAL - HOTELES"));


        pagerAdapter.addFragment(ScreenSlidePageFragment.newInstance("MIERCOLES 29 MARZO DEL 2017",
                "08:00 A 10:00 hrs", "HOTELES - MUNDO IMPERIAL",
                "17:30 A 19:00 hrs", "MUNDO IMPERIAL - HOTELES",
                "17:30 A 19:00 hrs", "MUNDO IMPERIAL - COCTEL CAMBIO DE ESTAFETA",
                "23:00 hrs", "COCTEL DE CAMBIO DE ESTAFETA - HOTELES"));

        pagerAdapter.addFragment(ScreenSlidePageFragment.newInstance("JUEVES 30 MARZO DEL 2017",
                "08:00 A 10:00 hrs", "HOTELES - MUNDO IMPERIAL",
                "13:00 A 15:00 hrs", "MUNDO IMPERIAL - HOTELES"));

        pager.setAdapter(pagerAdapter);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        centroid = new MarkerOptions()
                .position(new LatLng(16.7690453,-99.7685701))
                .title(getString(R.string.tianguis_turistico));

        buttons.add((TextView) findViewById(R.id.button_r1));
        buttons.add((TextView) findViewById(R.id.button_r2));
        buttons.add((TextView) findViewById(R.id.button_r3));
        buttons.add((TextView) findViewById(R.id.button_r4));
        buttons.add((TextView) findViewById(R.id.button_r5));

        paintRoute(route_1);
        mMap.addPolyline(polyline_r1);
    }

    private ArrayList<LatLng> decodePoly(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            //LatLng p = new LatLng((int) (((double) lat /1E5)* 1E6), (int) (((double) lng/1E5   * 1E6)));
            LatLng p = new LatLng((((double) lat / 1E5)),(((double) lng / 1E5)));

            poly.add(p);
        }
        return poly;
    }

    public void show_r1(View view) {
        cleanButtons((TextView) view);
        paintRoute(route_1);
        mMap.addPolyline(polyline_r1);
    }

    public void show_r2(View view) {
        cleanButtons((TextView) view);
        paintRoute(route_2);
        mMap.addPolyline(polyline_r2);
    }

    public void show_r3(View view) {
        cleanButtons((TextView) view);
        paintRoute(route_3);
        mMap.addPolyline(polyline_r3);
    }

    public void show_r4(View view) {
        cleanButtons((TextView) view);
        paintRoute(route_4);
        mMap.addPolyline(polyline_r4);
    }

    public void show_r5(View view) {
        cleanButtons((TextView) view);
        paintRoute(route_5);
        mMap.addPolyline(polyline_r5);
    }

    private void paintRoute(ArrayList<MarkerOptions> route) {
        mMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for(int i = 0; i < route.size(); i++){
            mMap.addMarker(route.get(i));
            builder.include(route.get(i).getPosition());
        }

        builder.include(centroid.getPosition());
        LatLngBounds bounds = builder.build();


        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, 100);
        mMap.animateCamera(cu);

        mMap.addMarker(centroid).showInfoWindow();
    }


    private void readData(){
        route_1.add(new MarkerOptions()
                .position(new LatLng(16.860058,-99.8861182))
                .title("Krystal Beach Acapulco"));

        route_1.add(new MarkerOptions()
                .position(new LatLng(16.8603297,-99.884895))
                .title("Ritz Acapulco"));

        route_1.add(new MarkerOptions()
                .position(new LatLng(16.8598945,-99.878627))
                .title("Crowne Plaza Acapulco"));

        route_1.add(new MarkerOptions()
                .position(new LatLng(16.8601718,-99.877822))
                .title("Playa Suites Acapulco"));

        route_1.add(new MarkerOptions()
                .position(new LatLng(16.8597489,-99.8764099))
                .title("Hotel Emporio Acapulco"));

        ArrayList<LatLng> puntos = parseResponse("routes/r1.json");
        polyline_r1 = new PolylineOptions().color(Color.BLUE).width(5);

        for(int i=0;i< puntos.size();i++){
            polyline_r1.add(new LatLng(puntos.get(i).latitude, puntos.get(i).longitude));
        }


        /*
            Fiesta+Americana+Villas+Acapulco/@
            Holiday+Inn+Resort+Acapulco/@
            El+Presidente/@
            Calinda+Beach/@
            Hotel+Acapulco+Malibú/@
         */

        route_2.add(new MarkerOptions()
                .position(new LatLng(16.856163,-99.8685981))
                .title("Fiesta Americana Villas Acapulco"));

        route_2.add(new MarkerOptions()
                .position(new LatLng(16.855807,-99.8658169))
                .title("Holiday Inn Resort Acapulco"));

        route_2.add(new MarkerOptions()
                .position(new LatLng(16.8560143,-99.8653123))
                .title("El Presidente"));

        route_2.add(new MarkerOptions()
                .position(new LatLng(16.8554509,-99.8632366))
                .title("Calinda Beach"));

        route_2.add(new MarkerOptions()
                .position(new LatLng(16.8548134,-99.8612423))
                .title("Hotel Acapulco Malibú"));


        puntos = parseResponse("routes/r2.json");
        polyline_r2 = new PolylineOptions().color(Color.BLUE).width(5);
        for(int i=0;i< puntos.size();i++){
            polyline_r2.add(new LatLng(puntos.get(i).latitude, puntos.get(i).longitude));
        }

        /*
        * 3
        * */

        route_3.add(new MarkerOptions()
                .position(new LatLng(16.8536849,-99.8601806))
                .title("Elcano Acapulco"));

        route_3.add(new MarkerOptions()
                .position(new LatLng(16.8524619,-99.8588347))
                .title("Hotel Copacabana Beach Acapulco"));

        route_3.add(new MarkerOptions()
                .position(new LatLng(16.8456541,-99.8516803))
                .title("Casa Inn Acapulco"));

        route_3.add(new MarkerOptions()
                .position(new LatLng(16.8428904,-99.8529776))
                .title("Grand Hotel Acapulco"));

        puntos = parseResponse("routes/r3.json");
        polyline_r3 = new PolylineOptions().color(Color.BLUE).width(5);

        for(int i=0;i< puntos.size();i++){
            polyline_r3.add(new LatLng(puntos.get(i).latitude, puntos.get(i).longitude));
        }

        //

        route_4.add(new MarkerOptions()
                .position(new LatLng(16.791727,-99.8247327))
                .title("Pierre Mundo Acapulco"));

        route_4.add(new MarkerOptions()
                .position(new LatLng(16.789367,-99.8165887))
                .title("Hotel Princess Mundo Imperial Riviera Diamante Acapulco"));

        route_4.add(new MarkerOptions()
                .position(new LatLng(16.7859844,-99.8030953))
                .title("Mayan Palace Acapulco"));

        puntos = parseResponse("routes/r4.json");
        polyline_r4 = new PolylineOptions().color(Color.BLUE).width(5);

        for(int i=0;i< puntos.size();i++){
            polyline_r4.add(new LatLng(puntos.get(i).latitude, puntos.get(i).longitude));
        }

        route_5.add(new MarkerOptions().position(new LatLng(16.826175,-99.8642527)).title("Hotel Las Brisas Acapulco"));
        route_5.add(new MarkerOptions().position(new LatLng(16.832768,-99.8607227)).title("Hotel Park Royal Acapulco"));
        route_5.add(new MarkerOptions().position(new LatLng(16.8150676,-99.863243)).title("Encanto Acapulco"));
        route_5.add(new MarkerOptions().position(new LatLng(16.810838,-99.8511457)).title("Camino Real Acapulco Diamante"));
        route_5.add(new MarkerOptions().position(new LatLng(16.792397,-99.8290767)).title("Quinta Real Acapulco"));
        route_5.add(new MarkerOptions().position(new LatLng(16.7862551,-99.7948142)).title("Holiday Inn Acapulco La Isla"));
        route_5.add(new MarkerOptions().position(new LatLng(16.7859844,-99.8030953)).title("Mayan Palace"));

        puntos = parseResponse("routes/r5.json");
        polyline_r5 = new PolylineOptions().color(Color.BLUE).width(5);

        for(int i=0;i< puntos.size();i++){
            polyline_r5.add(new LatLng(puntos.get(i).latitude, puntos.get(i).longitude));
        }
    }


    private void cleanButtons(TextView view){
        for(TextView button: buttons){
            button.setTextColor(Color.DKGRAY);
            button.setBackgroundResource(R.drawable.circle_empty);
        }

        if(view!=null){
            view.setBackgroundResource(R.drawable.circle_orange);
            view.setTextColor(Color.WHITE);
        }
    }

    private ArrayList<LatLng> parseResponse(String file){
        InputStream input;
        try {
            input = getAssets().open(file);

            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            String text = new String(buffer);

            JSONObject json = new JSONObject(text);

            String jsonArray = json.getJSONArray("routes").getJSONObject(0)
                    .getJSONObject("overview_polyline").getString("points");

            return decodePoly(jsonArray);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> fragments;

        private MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            this.fragments = new ArrayList<Fragment>();
        }

        private void addFragment(Fragment fragment) {
            this.fragments.add(fragment);
        }

        @Override
        public Fragment getItem(int arg0) {
            return this.fragments.get(arg0);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }

}
