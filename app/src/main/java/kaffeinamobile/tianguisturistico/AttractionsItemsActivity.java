package kaffeinamobile.tianguisturistico;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.adapters.AttractionsAdapter;
import kaffeinamobile.tianguisturistico.models.Attraction;
import kaffeinamobile.tianguisturistico.models.Category;
import kaffeinamobile.tianguisturistico.models.Sponsor;


public class AttractionsItemsActivity extends BaseActivity {

    private Category category;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attractions_items);

        setToolbar();
        activateUpButton();

        category = (Category) getIntent().getSerializableExtra("category");

        TextView title = (TextView) findViewById(R.id.sitios_category_title);
        title.setText(category.getName());

        ArrayList<Attraction> data = getData(category.getId(), category.getSmallIcon_id());
        AttractionsAdapter adapter = new AttractionsAdapter(this, data);

        ListView listView = (ListView) findViewById(R.id.listview_attractions);
        listView.setAdapter(adapter);
    }


    private ArrayList<Attraction> getData(String categoryId, int smallIcon){
        InputStream input;
        ArrayList<Attraction> attractions = new ArrayList<>();
        try {
            input = getAssets().open("attractions.json");

            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            String req = new String(buffer);

            JSONObject json = new JSONObject(req);
            JSONArray data = json.getJSONArray("data");

            JSONObject attractionObject;
            Attraction attraction;
            for(int i = 0; i < data.length(); i++ ){
                attractionObject = data.getJSONObject(i);

                if( categoryId != null && categoryId.equals(attractionObject.getString("category_id")) ){
                    attraction = new Attraction();

                    attraction.setAddress(attractionObject.getString("adress"));
                    attraction.setCategoryId(attractionObject.getString("category_id"));
                    attraction.setContact(attractionObject.getString("contact"));
                    attraction.setDescription(attractionObject.getString("description"));

                    attraction.setId(attractionObject.getString("id"));
                    attraction.setName(attractionObject.getString("name"));
                    attraction.setUrl(attractionObject.getString("url"));
                    attraction.setZone(attractionObject.getString("zone"));

                    attraction.setSmallIcon(smallIcon);
                    attraction.setParentPicture(category.getBackground_id());

                    attractions.add(attraction);
                }
            }

        } catch (IOException | JSONException ignored) {}

        return attractions;
    }


}
