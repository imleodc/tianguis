package kaffeinamobile.tianguisturistico;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.BaseActivity;
import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.adapters.PremiosAdapter;
import kaffeinamobile.tianguisturistico.models.Premios;

import static android.R.attr.data;


public class PremiosActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premios);

        setToolbar();
        activateUpButton();

        TextView stepsScore = (TextView) findViewById(R.id.steps_score);
        TextView photosScore = (TextView) findViewById(R.id.photos_score);
        TextView appointmentScore = (TextView) findViewById(R.id.appointments_score);

        Intent intent = getIntent();
        String user_steps = intent.getStringExtra("user_steps");
        String user_photos = intent.getStringExtra("user_photos");
        String user_appointments = intent.getStringExtra("user_appointments");

        stepsScore.setText(user_steps);
        photosScore.setText(user_photos);
        appointmentScore.setText(user_appointments);
    }

}
