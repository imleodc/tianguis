package kaffeinamobile.tianguisturistico.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by lcastaneda005 on 12/03/2017.
 */

public class CustomFontButton extends android.support.v7.widget.AppCompatButton {

    public CustomFontButton(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/SoberanaSans-Bold.otf", context);
        setTypeface(customFont);
    }
}
