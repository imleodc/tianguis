package kaffeinamobile.tianguisturistico.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class SoberanaSansUltraFontTextView extends android.support.v7.widget.AppCompatTextView{

    public SoberanaSansUltraFontTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public SoberanaSansUltraFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public SoberanaSansUltraFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/SoberanaSans-Ultra.otf", context);
        setTypeface(customFont);
    }

}
