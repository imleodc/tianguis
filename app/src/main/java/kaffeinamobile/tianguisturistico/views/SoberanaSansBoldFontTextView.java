package kaffeinamobile.tianguisturistico.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by lcastaneda005 on 11/03/2017.
 */

public class SoberanaSansBoldFontTextView extends android.support.v7.widget.AppCompatTextView{

    public SoberanaSansBoldFontTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public SoberanaSansBoldFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public SoberanaSansBoldFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/SoberanaSans-Bold.otf", context);
        setTypeface(customFont);
    }

}
