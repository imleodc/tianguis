package kaffeinamobile.tianguisturistico.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by lcastaneda005 on 12/03/2017.
 */

public class SoberanaSansBlackFontTextView extends android.support.v7.widget.AppCompatTextView{

    public SoberanaSansBlackFontTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public SoberanaSansBlackFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public SoberanaSansBlackFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/SoberanaSans-Black.otf", context);
        setTypeface(customFont);
    }
}
