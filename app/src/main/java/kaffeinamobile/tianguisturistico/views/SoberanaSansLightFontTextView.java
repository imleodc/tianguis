package kaffeinamobile.tianguisturistico.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

/**
 * Created by lcastaneda005 on 16/03/2017.
 */

public class SoberanaSansLightFontTextView extends AppCompatTextView{

    public SoberanaSansLightFontTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public SoberanaSansLightFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public SoberanaSansLightFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("fonts/SoberanaSans-Light.otf", context);
        setTypeface(customFont);
    }

}
