package kaffeinamobile.tianguisturistico;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

import kaffeinamobile.tianguisturistico.models.Contact;
import kaffeinamobile.tianguisturistico.models.Sponsor;
import kaffeinamobile.tianguisturistico.services.StepDetectorService;


public class TianguisApp extends Application{

    public static final String TIANGUIS_APP_MEMORY = "TIANGUIS_APP_MEMORY";

    public static final String ID_CONTACTS_FAV = "FAV_CONTACTS";

    private ArrayList<Contact> contactsFav = new ArrayList<>();

    private ArrayList<Sponsor> sponsors;
    public static ArrayList<Contact> contacts;

    @Override
    public void onCreate() {
        super.onCreate();

        Fresco.initialize(this);

        readContacts();
        buildFavs();
    }

    public static Contact findContactById(String delegate_id){
        for(int i = 0; i < contacts.size(); i++){
            if(contacts.get(i).getDelegate_id().equals(delegate_id)){
                return contacts.get(i);
            }
        }
        return null;
    }

    public ArrayList<Contact> getContactsFav() {
        return contactsFav;
    }

    public boolean addContactToFav(Contact contact){
        if(contactsFav.contains(contact)){
            return false;
        }

        contactsFav.add(contact);

        SharedPreferences settings = getApplicationContext().getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);
        String delegate_id = settings.getString(ID_CONTACTS_FAV, "");

        delegate_id = (delegate_id.isEmpty()) ? contact.getDelegate_id(): delegate_id+"," + contact.getDelegate_id();


        SharedPreferences.Editor edit = settings.edit();
        edit.putString(ID_CONTACTS_FAV, delegate_id);
        edit.commit();

        return true;
    }

    public void buildFavs() {
        contactsFav = new ArrayList<>();

        SharedPreferences settings = getApplicationContext().getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);
        String delegates = settings.getString(ID_CONTACTS_FAV, "");

        String[] delegate_ids = delegates.split(",");

        Contact contact;
        for(int i = 0; i < contacts.size(); i++){
            contact = contacts.get(i);

            for (String delegate_id : delegate_ids) {
                if (contact.getDelegate_id().equals(delegate_id)) {
                    contactsFav.add(contact);
                    break;
                }
            }
        }
    }

    public ArrayList<Sponsor> getSponsors() {
        if(sponsors==null){
            sponsors = new ArrayList<>();

            InputStream input;
            try {
                input = getAssets().open("sponsors.json");

                int size = input.available();
                byte[] buffer = new byte[size];
                input.read(buffer);
                input.close();

                String text = new String(buffer);

                JSONObject json = new JSONObject(text);
                JSONArray data = json.getJSONArray("data");

                JSONObject record;
                Sponsor sponsor;
                for(int i = 0; i < data.length(); i++ ){
                    record = data.getJSONObject(i);
                    sponsor = new Sponsor();

                    sponsor.setId(record.getString("id"));
                    sponsor.setPriority(record.getInt("priority"));
                    sponsor.setOrder(record.getInt("order"));
                    sponsor.setName(record.getString("name"));
                    sponsor.setUrl(record.getString("url"));


                    switch (sponsor.getId()){
                        case "1":
                            sponsor.setImage(R.drawable.aeromexico);
                            break;
                        case "2":
                            sponsor.setImage(R.drawable.ado);
                            break;
                        case "3":
                            sponsor.setImage(R.drawable.estrella_oro);
                            break;
                        case "4":
                            sponsor.setImage(R.drawable.mundo_cuervo);
                            break;
                        case "5":
                            sponsor.setImage(R.drawable.cafe_gourmet);
                            break;
                        case "6":
                            sponsor.setImage(R.drawable.infinitum);
                            break;
                        case "7":
                            sponsor.setImage(R.drawable.coca_cola);
                            break;
                        default:
                            sponsor.setImage(R.drawable.coca_cola);
                            break;
                    }

                    sponsors.add(sponsor);
                }

            } catch (IOException | JSONException ignored) {}
        }

        return sponsors;
    }


    private void readContacts(){
        contacts = new ArrayList<>();
        InputStream input;
        try {
            input = getAssets().open("directory.json");

            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            String text = new String(buffer);

            JSONObject json = new JSONObject(text);
            JSONArray data = json.getJSONArray("data");

            JSONObject record;
            Contact contact;
            String group;
            for(int i = 0; i < data.length(); i++ ){
                record = data.getJSONObject(i);

                group = record.getString("name");
                JSONArray delegates = record.getJSONArray("delegates");
                for(int j = 0; j < delegates.length(); j++){
                    JSONObject delegatesJSONObject = delegates.getJSONObject(j);

                    contact = new Contact();

                    contact.setName(delegatesJSONObject.getString("name"));
                    contact.setMember_id(delegatesJSONObject.getString("member_id"));
                    contact.setDelegate_id(delegatesJSONObject.getString("delegate_id"));
                    contact.setEmail(delegatesJSONObject.getString("email"));

                    //check
                    contact.setPhone(delegatesJSONObject.getString("email"));

                    contact.setGroup(group);
                    contact.setFirstToShow(j==0);

                    contacts.add(contact);
                }
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

}
