package kaffeinamobile.tianguisturistico;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import kaffeinamobile.tianguisturistico.net.HttpGetRequest;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TimeLineLoginActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline_login);

        setToolbar();

        SharedPreferences settings = getApplicationContext().getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);
        String delegate_id = settings.getString("delegate_id", "");

        EditText userInput = (EditText) findViewById(R.id.user_id_login);
        userInput.setText(delegate_id);
    }

    public void ingresar(View view) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", getString(R.string.loading), true, false);
        ringProgressDialog.show();

        EditText userInput = (EditText) findViewById(R.id.user_id_login);
        final String delegate_id = userInput.getText().toString();

         login(delegate_id, new Callback<ResponseBody>() {
             @Override
             public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                 ringProgressDialog.dismiss();
                 if(response.isSuccessful()){
                     try {
                         String result = response.body().string();

                         JSONObject json = new JSONObject(result);

                         if(json.getBoolean("status")){
                             SharedPreferences settings = getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, MODE_PRIVATE);
                             SharedPreferences.Editor edit = settings.edit();
                             edit.putString("delegate_id", delegate_id);
                             edit.commit();

                             Intent intent = new Intent(getApplicationContext(), TimeLineActivity.class);
                             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                             getApplicationContext().startActivity(intent);
                         }else {
                             throw new JSONException("");
                         }
                     } catch (IOException | JSONException e) {
                         Toast.makeText(getApplicationContext(), getString(R.string.error_login), Toast.LENGTH_SHORT).show();
                     }
                 }
             }

             @Override
             public void onFailure(Call<ResponseBody> call, Throwable t) {
                 Toast.makeText(getApplicationContext(), getString(R.string.error_login), Toast.LENGTH_SHORT).show();
                 ringProgressDialog.dismiss();
             }
         });
    }
}
