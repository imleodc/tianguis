package kaffeinamobile.tianguisturistico;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.adapters.ServiciosEmergenciaAdapter;
import kaffeinamobile.tianguisturistico.models.EmergencyService;
import kaffeinamobile.tianguisturistico.net.HttpGetRequest;

public class ServiciosEmergenciaActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicios_emergencia);

        setToolbar();

        ArrayList<EmergencyService> contacts = parseData();

        ServiciosEmergenciaAdapter adapter = new ServiciosEmergenciaAdapter(this, contacts);

        ListView listView = (ListView)findViewById(R.id.listview_servicios_emergencia);
        listView.setAdapter(adapter);
    }

    private ArrayList<EmergencyService> parseData(){
        try {
            /*
            String endPoint = "http://movil-projects.com/tt2017/api/emergency-services";

            HttpGetRequest getRequest = new HttpGetRequest();
            String result = getRequest.execute(endPoint).get();
            */
            String result = "{\"status\":true,\"data\":[{\"id\":\"1\",\"name\":\"911\",\"description\":\"Comunica a todas las dependencias de emergencias\",\"phone\":\"911\"},{\"id\":\"2\",\"name\":\"Centro de Atenci\\u00f3n y Protecci\\u00f3n al Turista Acapulco\",\"description\":\"Centro de Atenci\\u00f3n y Protecci\\u00f3n al Turista Acapulco\",\"phone\":\"(744) 4811854\"}]}";
            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("data");

            JSONObject record;
            ArrayList<EmergencyService> emergencyServices = new ArrayList<>();
            EmergencyService service;
            for(int i = 0; i < data.length(); i++ ){
                record = data.getJSONObject(i);

                service = new EmergencyService();
                service.setName(record.getString("name"));
                service.setDescription(record.getString("description"));
                service.setPhone(record.getString("phone"));

                emergencyServices.add(service);
            }

            return emergencyServices;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
