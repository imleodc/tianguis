package kaffeinamobile.tianguisturistico;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import kaffeinamobile.tianguisturistico.models.Meeting;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CheckAppointmentActivity extends BaseActivity {

    private Meeting meeting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_meeting);

        setToolbar();
        activateUpButton();

        meeting = (Meeting) getIntent().getSerializableExtra("meeting");

        if(meeting.getAppointmentCode()!=null){
            findViewById(R.id.validateLayout).setVisibility(View.GONE);

            TextView codeTextView = (TextView) findViewById(R.id.meeting_code_text);
            codeTextView.setText(meeting.getAppointmentCode());
            codeTextView.setVisibility(View.VISIBLE);
        }
    }

    public void validateMeeting(View view) {
        ServiceTianguis serviceTianguis = new ServiceTiaguisImp();

        String code = ((EditText) findViewById(R.id.user_id_login)).getText().toString();

        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", getString(R.string.loading), true, false);
        ringProgressDialog.show();
        serviceTianguis.confirmAppointment(code, meeting.getAppointmentDelegate_id(), meeting.getSlotNumber(), new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ringProgressDialog.dismiss();

                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        JSONObject json = new JSONObject(result);

                        if(json.getBoolean("status")){
                            Intent intent = new Intent(getApplicationContext(), SuccessfulAppointment.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            finish();
                        }else{
                            JSONObject errors = json.getJSONObject("errors");
                            String error = errors.isNull("code") ? "Error":errors.getString("code");
                            Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        Toast.makeText(getApplicationContext(), getString(R.string.general_error), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getString(R.string.general_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ringProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.general_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
