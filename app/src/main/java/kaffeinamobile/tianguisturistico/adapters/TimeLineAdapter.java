package kaffeinamobile.tianguisturistico.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Picture;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.TianguisApp;
import kaffeinamobile.tianguisturistico.models.Category;
import kaffeinamobile.tianguisturistico.models.Pictures;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeLineAdapter extends ArrayAdapter<Pictures> {

    private ServiceTianguis serviceTianguis = new ServiceTiaguisImp();
    private LayoutInflater inflater;

    public TimeLineAdapter(@NonNull Context context, @NonNull List<Pictures> objects) {
        super(context, 0, objects);

        inflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final Pictures picture = getItem(position);

        final ViewHolder viewHolder;
        View view = inflater.inflate(R.layout.view_timeline_item, parent, false);

        viewHolder = new ViewHolder();
        viewHolder.imageView = (ImageView)view.findViewById(R.id.timeline_picture);
        viewHolder.likesView = (TextView) view.findViewById(R.id.timeline_likes_text);
        viewHolder.like_picture = (LinearLayout) view.findViewById(R.id.like_picture);

        viewHolder.likesView.setText(String.valueOf(picture.getLikes()));

        Picasso.with(getContext()).load(picture.getImage_thumb_url()).into(viewHolder.imageView);

        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Pictures> aux = new ArrayList<>();
                aux.add(picture);

                new ImageViewer.Builder<>(getContext(), aux)
                        .setFormatter(new ImageViewer.Formatter<Pictures>() {
                            @Override
                            public String format(Pictures picture) {
                                return picture.getImage_thumb_url();
                            }
                        })
                        .show();
            }
        });

        viewHolder.like_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(!picture.getLiked()){
                    final ProgressDialog ringProgressDialog = ProgressDialog.show(getContext(), "", getContext().getString(R.string.loading), true, false);
                    ringProgressDialog.show();

                    SharedPreferences settings = getContext().getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);
                    String delegate_id = settings.getString("delegate_id", "");

                    serviceTianguis.likePhoto(delegate_id, picture.getId(), new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if(response.isSuccessful()){
                                try {
                                    JSONObject responseJSON = new JSONObject(response.body().string());

                                    if(responseJSON.getBoolean("status")){
                                        picture.setLiked(true);
                                        viewHolder.likesView.setText(String.valueOf(picture.getLikes()+1));
                                    }

                                } catch (JSONException | IOException e) {
                                    Toast.makeText(getContext(), getContext().getString(R.string.error_like_picture), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getContext(), getContext().getString(R.string.error_like_picture), Toast.LENGTH_SHORT).show();
                            }

                            ringProgressDialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            ringProgressDialog.dismiss();
                            Toast.makeText(getContext(), getContext().getString(R.string.error_like_picture), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });

        return view;
    }


    private static class ViewHolder {
        ImageView imageView;
        TextView likesView;
        LinearLayout like_picture;
    }

}
