package kaffeinamobile.tianguisturistico.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.models.Contact;
import kaffeinamobile.tianguisturistico.models.EmergencyService;

/**
 * Created by lcastaneda005 on 12/03/2017.
 */

public class ServiciosEmergenciaAdapter extends BaseAdapter {

    private ArrayList<EmergencyService> emergencyServicesList;
    private LayoutInflater inflater;

    public ServiciosEmergenciaAdapter(Context context, ArrayList<EmergencyService> emergencyServicesList) {
        this.emergencyServicesList = emergencyServicesList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return emergencyServicesList.size();
    }

    @Override
    public Object getItem(int position) {
        return emergencyServicesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        final EmergencyService service = emergencyServicesList.get(position);

        convertView = inflater.inflate(R.layout.view_servicio_emergencia_item, parent, false);

        viewHolder = new ViewHolder();
        viewHolder.title = (TextView) convertView.findViewById(R.id.servicio_emergencia_title);
        viewHolder.description = (TextView) convertView.findViewById(R.id.servicio_emergencia_desc);
        viewHolder.button = (ImageView) convertView.findViewById(R.id.servicio_emergencia_button);

        viewHolder.title.setText(service.getName());
        viewHolder.description.setText(service.getDescription());
        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse( "tel:" + service.getPhone()));

                inflater.getContext().startActivity(callIntent);
            }
        });

        return convertView;
    }

    private static class ViewHolder{
        TextView title;
        TextView description;
        ImageView button;
    }
}
