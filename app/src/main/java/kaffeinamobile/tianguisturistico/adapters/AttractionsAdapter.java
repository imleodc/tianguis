package kaffeinamobile.tianguisturistico.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.AttractionActivity;
import kaffeinamobile.tianguisturistico.AttractionsItemsActivity;
import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.models.Attraction;


public class AttractionsAdapter extends BaseAdapter {
    private ArrayList<Attraction> attractions;
    private Context context;
    private LayoutInflater inflater;

    public AttractionsAdapter(Context context, ArrayList<Attraction> attractions){
        this.context = context;
        this.attractions = attractions;

        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return attractions.size();
    }

    @Override
    public Object getItem(int position) {
        return attractions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Attraction attraction = attractions.get(position);
        convertView = inflater.inflate(R.layout.view_attractions_item, parent, false);

        ImageView icon = (ImageView)convertView.findViewById(R.id.attraction_category_image);
        TextView name = (TextView) convertView.findViewById(R.id.attraction_name);


        name.setText(attraction.getName());
        Picasso.with(context).load(attraction.getSmallIcon()).into(icon);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AttractionActivity.class);
                intent.putExtra("attraction", attraction);

                /*
                intent.putExtra("attraction_name", attraction.getName());
                intent.putExtra("attraction_tel", attraction.getContact());
                intent.putExtra("attraction_url", attraction.getUrl());
                intent.putExtra("attraction_desc", attraction.getDescription());
                */

                context.startActivity(intent);
            }
        });
        return convertView;
    }
}
