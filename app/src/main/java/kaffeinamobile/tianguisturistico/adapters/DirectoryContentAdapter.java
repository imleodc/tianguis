package kaffeinamobile.tianguisturistico.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import kaffeinamobile.tianguisturistico.ContactActivity;
import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.TianguisApp;
import kaffeinamobile.tianguisturistico.models.Contact;

public class DirectoryContentAdapter extends BaseAdapter {

    private boolean shoAsFav;
    private Context mContext;
    private LayoutInflater inflater;
    private List<Contact> filterList;
    private ArrayList<Contact> fullList;


    public DirectoryContentAdapter(Context context, List<Contact> contactsList) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);

        filterList = contactsList;
        fullList = new ArrayList<>();
        fullList.addAll(contactsList);
    }

    public DirectoryContentAdapter(Context context, List<Contact> contactsList, boolean shoAsFav) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.shoAsFav = shoAsFav;

        filterList = contactsList;
        fullList = new ArrayList<>();
        fullList.addAll(contactsList);
    }


    private void itemListener(int index){
        Intent intent = new Intent(mContext, ContactActivity.class);

        intent.putExtra("contact", filterList.get(index));
        mContext.startActivity(intent);
    }


    @Override
    public int getCount() {
        return filterList.size();
    }

    @Override
    public Contact getItem(int position) {
        return filterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, final ViewGroup parent) {
        final ViewHolder holder;

        Contact contact = filterList.get(position);

        holder = new ViewHolder();
        view = inflater.inflate(R.layout.view_directory_item, parent, false);

        holder.contact_name = (TextView) view.findViewById(R.id.contact_name);
        holder.contact_group = (TextView) view.findViewById(R.id.contact_group);
        holder.groupTitle = (TextView) view.findViewById(R.id.group_header);
        holder.deleteFav = (ImageView) view.findViewById(R.id.delete_fav);

        holder.contact_name.setText(contact.getName());
        holder.contact_group.setText(contact.getGroup());

        if(contact.getFirstToShow()){
            holder.groupTitle.setText(contact.getGroup());
            holder.groupTitle.setVisibility(View.VISIBLE);
        }else{
            holder.groupTitle.setVisibility(View.GONE);
        }

        if(shoAsFav){
            holder.deleteFav.setVisibility(View.VISIBLE);
            holder.groupTitle.setVisibility(View.GONE);

            holder.deleteFav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeFromFavs(position);
                }
            });
        }

        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                itemListener(position);
            }
        });

        return view;
    }


    private void removeFromFavs(int position){
        Contact removed = filterList.remove(position);

        SharedPreferences sharedPreferences = mContext.getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);
        String delegates = sharedPreferences.getString(TianguisApp.ID_CONTACTS_FAV, "");

        List<String> delegates_id = Arrays.asList(delegates.split(","));

        String aux = "";
        for(int i = 0; i < delegates_id.size(); i++){
            if(!delegates_id.get(i).equals(removed.getDelegate_id())){
                if( aux.length() > 0 ) {
                    aux += ",";
                }
                aux += delegates_id.get(i);
            }
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(TianguisApp.ID_CONTACTS_FAV, aux);
        edit.commit();

        notifyDataSetChanged();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault()).replaceAll(" $","");
        filterList.clear();

        if (charText.length() == 0) {
            filterList.addAll(fullList);
        } else {
            for (Contact contact: fullList) {
                if(contact.getName().toLowerCase(Locale.getDefault()).contains(charText) || contact.getGroup().toLowerCase(Locale.getDefault()).contains(charText)){
                    filterList.add(contact);
                }
            }
        }
        notifyDataSetChanged();
    }

    private class ViewHolder {
        TextView contact_name;
        TextView contact_group;
        TextView groupTitle;
        ImageView deleteFav;
    }

}