package kaffeinamobile.tianguisturistico.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import kaffeinamobile.tianguisturistico.AttractionsItemsActivity;
import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.models.Category;

public class AttractionsCategoryAdapter extends ArrayAdapter<Category> {

    public AttractionsCategoryAdapter(@NonNull Context context, @NonNull List<Category> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Category category = getItem(position);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_sitios_item, parent, false);

        ImageView categoryPicture = (ImageView) view.findViewById(R.id.category_picture);
        ImageView categoryThumbnail = (ImageView) view.findViewById(R.id.category_thumbnail);
        TextView categoryName = (TextView) view.findViewById(R.id.category_name);

        categoryName.setText(category.getName());

        Picasso.with(getContext()).load(category.getBackground_id()).fit().into(categoryPicture);
        Picasso.with(getContext()).load(category.getSmallIcon_id()).into(categoryThumbnail);



        categoryPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AttractionsItemsActivity.class);
                intent.putExtra("category", category);
                getContext().startActivity(intent);
            }
        });

        return view;
    }

}
