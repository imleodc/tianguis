package kaffeinamobile.tianguisturistico.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.models.Premios;

public class PremiosAdapter extends BaseAdapter{

    private ArrayList<Premios> premiosList;
    private LayoutInflater inflater;

    public PremiosAdapter(Context context, ArrayList<Premios> premiosList){
        this.premiosList = premiosList;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return premiosList.size();
    }

    @Override
    public Object getItem(int position) {
        return premiosList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Premios premios = premiosList.get(position);
        convertView = inflater.inflate(R.layout.view_premios_item, parent, false);

        viewHolder = new ViewHolder();
        viewHolder.completeBadge = (ImageView)convertView.findViewById(R.id.premios_badge_complete);
        viewHolder.premiosText = (TextView) convertView.findViewById(R.id.premios_badge_text);
        viewHolder.scoreText = (TextView) convertView.findViewById(R.id.premios_badge_score);


        viewHolder.premiosText.setText(premios.getText());
        if(!premios.getCompleted()){
            viewHolder.completeBadge.setVisibility(View.GONE);

            viewHolder.scoreText.setVisibility(View.VISIBLE);
            viewHolder.scoreText.setText(premios.getScore());
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView completeBadge;
        TextView premiosText;
        TextView scoreText;
    }
}
