package kaffeinamobile.tianguisturistico.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.AgendaActivity;
import kaffeinamobile.tianguisturistico.CheckAppointmentActivity;
import kaffeinamobile.tianguisturistico.ContactActivity;
import kaffeinamobile.tianguisturistico.R;
import kaffeinamobile.tianguisturistico.SuccessfulAppointment;
import kaffeinamobile.tianguisturistico.TianguisApp;
import kaffeinamobile.tianguisturistico.models.Contact;
import kaffeinamobile.tianguisturistico.models.Meeting;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AgendaAdapter extends ArrayAdapter<Meeting>{

    private LayoutInflater inflater;
    private Context context;

    public AgendaAdapter(Context context, ArrayList<Meeting> meetings){
        super(context, 0, meetings);
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final ViewHolder viewHolder;

        convertView = inflater.inflate(R.layout.view_agenda_item, parent, false);

        viewHolder = new ViewHolder();
        viewHolder.imageIcon = (ImageView) convertView.findViewById(R.id.contact_icon);
        viewHolder.imageCheck = (ImageView) convertView.findViewById(R.id.meeting_check_blue);
        viewHolder.group = (TextView) convertView.findViewById(R.id.agenda_meeting_group);
        viewHolder.name = (TextView) convertView.findViewById(R.id.agenda_meeting_name);
        viewHolder.time = (TextView) convertView.findViewById(R.id.agenda_meeting_time);

        final Meeting meeting = getItem(position);

        viewHolder.time.setText(meeting.getTime());
        if(meeting.getFilled()){
            Picasso.with(context).load(R.mipmap.contacto_icon_blue).into(viewHolder.imageIcon);

            viewHolder.imageCheck.setVisibility(View.VISIBLE);
            viewHolder.name.setText(meeting.getName());
            viewHolder.group.setText(meeting.getGroup());

            viewHolder.imageIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startDirectory(meeting.getAppointmentDelegate_id());
                }
            });

            viewHolder.imageCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startCodeCheck(meeting);
                }
            });
        }

        return convertView;
    }

    private void startCodeCheck(final Meeting meeting) {
        final SharedPreferences settings = context.getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);

        final String delegate = settings.getString("delegate_id", "");
        final String delegate_s = meeting.getAppointmentDelegate_id();
        final String slot = meeting.getSlotNumber();


        final ProgressDialog ringProgressDialog = ProgressDialog.show(context, "", context.getString(R.string.loading), true, false);
        ringProgressDialog.show();

        ServiceTianguis serviceTianguis = new ServiceTiaguisImp();
        serviceTianguis.getAppointment(delegate, delegate_s, slot, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ringProgressDialog.dismiss();

                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        JSONObject json = new JSONObject(result);
                        if(json.getBoolean("status")){
                            JSONObject data = json.getJSONObject("data");

                            if(!data.isNull("code")){
                                String code = data.getString("code");
                                meeting.setAppointmentCode(code);

                                SharedPreferences settings = context.getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, Context.MODE_PRIVATE);
                                JSONArray array = new JSONArray(settings.getString("appointment_codes", "[]"));

                                JSONObject aux = new JSONObject();
                                aux.put("delegate_1_id", delegate);
                                aux.put("delegate_2_id", delegate_s);
                                aux.put("slot_number", slot);
                                aux.put("code", code);

                                array.put(aux);

                                SharedPreferences.Editor edit = settings.edit();
                                edit.putString("appointment_codes", array.toString());
                                edit.commit();
                            }else if(data.getString("status_code").equals("confirmed")){
                                Intent intent = new Intent(getContext(), SuccessfulAppointment.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getContext().startActivity(intent);

                                return;
                            }else{
                                try {
                                    JSONArray array = new JSONArray(settings.getString("appointment_codes", "[]"));

                                    for(int i = 0; i < array.length(); i++){
                                        JSONObject jsonObject = array.getJSONObject(i);

                                        String delegate_1_id = jsonObject.getString("delegate_1_id");
                                        String delegate_2_id = jsonObject.getString("delegate_2_id");
                                        String slot_id = jsonObject.getString("slot_number");

                                        if(delegate_1_id.equals(delegate)){
                                            if(delegate_2_id.equals(delegate_s)){
                                                if(slot_id.equals(slot_id)){
                                                    String code = jsonObject.getString("code");
                                                    meeting.setAppointmentCode(code);
                                                }
                                            }
                                        }
                                    }
                                } catch (JSONException ignored) {}
                            }

                            startCodeActivity(meeting);
                        }else{
                            JSONObject errors = json.getJSONObject("errors");
                            String error = !errors.isNull("delegate_1_id") ? errors.getString("delegate_1_id"):!errors.isNull("delegate_2_id")?errors.getString("delegate_2_id"):errors.getString("slot_number");
                            showMessage(error);
                        }
                    } catch (JSONException | IOException e) {
                        showMessage(context.getString(R.string.general_error));
                    }

                }else{
                    showMessage(context.getString(R.string.general_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ringProgressDialog.dismiss();
                showMessage(context.getString(R.string.error_login));
            }
        });
    }

    private void startCodeActivity(Meeting meeting){
        Intent intent = new Intent(context, CheckAppointmentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("meeting", meeting);

        context.startActivity(intent);
    }

    private void startDirectory(String delegate_id) {
        Contact contact = TianguisApp.findContactById(delegate_id);

        if(contact!=null){
            Intent intent = new Intent(context, ContactActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            intent.putExtra("contact", contact);

            context.startActivity(intent);
        }else{
            showMessage(context.getString(R.string.error_delegate_not_found));
        }
    }

    private void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private static class ViewHolder{
        ImageView imageIcon;
        ImageView imageCheck;
        TextView group;
        TextView name;
        TextView time;
    }
}
