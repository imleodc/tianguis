package kaffeinamobile.tianguisturistico.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kaffeinamobile.tianguisturistico.R;

public class ScreenSlidePageFragment extends Fragment {

    private static final String TITLE_KEY = "title_general";

    private static final String TIME_1_KEY = "time_1";
    private static final String TITLE_1_KEY = "title_1";

    private static final String TIME_2_KEY = "time_2";
    private static final String TITLE_2_KEY = "title_2";

    private static final String TIME_3_KEY = "time_3";
    private static final String TITLE_3_KEY = "title_3";

    private static final String TIME_4_KEY = "time_4";
    private static final String TITLE_4_KEY = "title_4";

    private String title;

    private String time_1;
    private String title_1;
    private String time_2;
    private String title_2;
    private String time_3;
    private String title_3;
    private String time_4;
    private String title_4;

    public static ScreenSlidePageFragment newInstance(String title, String time_1, String title_1, String time_2, String title_2) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();

        Bundle bundle = new Bundle();
        bundle.putString(TITLE_KEY, title);

        bundle.putString(TIME_1_KEY, time_1);
        bundle.putString(TITLE_1_KEY, title_1);

        bundle.putString(TIME_2_KEY, time_2);
        bundle.putString(TITLE_2_KEY, title_2);

        fragment.setArguments(bundle);
        fragment.setRetainInstance(true);

        return fragment;
    }

    public static ScreenSlidePageFragment newInstance(String title, String time_1, String title_1, String time_2, String title_2, String time_3, String title_3, String time_4, String title_4) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();

        Bundle bundle = new Bundle();
        bundle.putString(TITLE_KEY, title);

        bundle.putString(TIME_1_KEY, time_1);
        bundle.putString(TITLE_1_KEY, title_1);

        bundle.putString(TIME_2_KEY, time_2);
        bundle.putString(TITLE_2_KEY, title_2);

        bundle.putString(TIME_3_KEY, time_3);
        bundle.putString(TITLE_3_KEY, title_3);

        bundle.putString(TIME_4_KEY, time_4);
        bundle.putString(TITLE_4_KEY, title_4);

        fragment.setArguments(bundle);
        fragment.setRetainInstance(true);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.title = (getArguments() != null) ? getArguments().getString(TITLE_KEY):"";

        this.time_1 = (getArguments() != null) ? getArguments().getString(TIME_1_KEY):"";
        this.title_1 = (getArguments() != null) ? getArguments().getString(TITLE_1_KEY):"";

        this.time_2 = (getArguments() != null) ? getArguments().getString(TIME_2_KEY):"";
        this.title_2 = (getArguments() != null) ? getArguments().getString(TITLE_2_KEY):"";

        this.time_3 = (getArguments() != null) ? getArguments().getString(TIME_3_KEY):"";
        this.title_3 = (getArguments() != null) ? getArguments().getString(TITLE_3_KEY):"";

        this.time_4 = (getArguments() != null) ? getArguments().getString(TIME_4_KEY):"";
        this.title_4 = (getArguments() != null) ? getArguments().getString(TITLE_4_KEY):"";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.pager_layout_item, container, false);

        ((TextView) rootView.findViewById(R.id.pager_title)).setText(this.title);

        ((TextView) rootView.findViewById(R.id.time_1)).setText(this.time_1);
        ((TextView) rootView.findViewById(R.id.title_1)).setText(this.title_1);

        ((TextView) rootView.findViewById(R.id.time_2)).setText(this.time_2);
        ((TextView) rootView.findViewById(R.id.title_2)).setText(this.title_2);

        ((TextView) rootView.findViewById(R.id.time_3)).setText(this.time_3);
        ((TextView) rootView.findViewById(R.id.title_3)).setText(this.title_3);

        ((TextView) rootView.findViewById(R.id.time_4)).setText(this.time_4);
        ((TextView) rootView.findViewById(R.id.title_4)).setText(this.title_4);

        if(this.time_3 != null && !this.time_3.isEmpty()){
            rootView.findViewById(R.id.calendar_row_3).setVisibility(View.VISIBLE);
        }

        if(this.time_4 != null && !this.time_4.isEmpty()){
            rootView.findViewById(R.id.calendar_row_4).setVisibility(View.VISIBLE);
        }

        // Change the background color
        //rootView.setBackgroundColor(this.color);

        return rootView;

    }
}