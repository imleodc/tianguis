package kaffeinamobile.tianguisturistico;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kaffeinamobile.tianguisturistico.adapters.TimeLineAdapter;
import kaffeinamobile.tianguisturistico.models.Pictures;
import kaffeinamobile.tianguisturistico.net.HttpGetRequest;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TimeLineActivity extends BaseActivity{

    private ListView timelineView;
    private TimeLineAdapter timelineAdapter;
    private ArrayList<Pictures> timelineList;

    private boolean showPopular = true;

    private String delegate_id;

    private ServiceTianguis serviceTianguis = new ServiceTiaguisImp();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_line);

        setToolbar();
        activateUpButton();


        SharedPreferences settings = getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, MODE_PRIVATE);
        delegate_id = settings.getString("delegate_id", "");

        timelineView = (ListView) findViewById(R.id.pictures_listview);

        timelineList = parseData("http://movil-projects.com/tt2017/api/gallery?delegate_id="+delegate_id);

        timelineAdapter = new TimeLineAdapter(this, (List<Pictures>) timelineList.clone());
        timelineView.setAdapter(timelineAdapter);
    }

    @Override
    protected void onRestart() {
        timelineList = parseData("http://movil-projects.com/tt2017/api/gallery?delegate_id="+delegate_id);

        timelineAdapter.clear();
        timelineAdapter.addAll((List<Pictures>) timelineList.clone());

        findViewById(R.id.button_populares).setAlpha(1);

        super.onRestart();
    }

    private ArrayList<Pictures> parseData(String endPoint){
        try {
            HttpGetRequest getRequest = new HttpGetRequest();

            String result = getRequest.execute(endPoint).get();

            JSONObject json = new JSONObject(result);
            JSONArray data = json.getJSONArray("data");

            JSONObject record;
            Pictures picture;
            ArrayList<Pictures> timelineList = new ArrayList<>();
            for(int i = 0; i < data.length(); i++ ){
                record = data.getJSONObject(i);

                picture = new Pictures(record.getString("image_url"), record.getInt("likes"), record.getString("image_medium_url"));
                picture.setId(record.getString("id"));
                picture.setLiked(record.getInt("liked")>0);

                timelineList.add(picture);
            }

            return timelineList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void startPictureActivity(View view) {
        Intent pictureItent = new Intent(view.getContext(), TakePictureActvity.class);
        startActivity(pictureItent);
    }

    public void showPopulares(View view) {
        if(showPopular){
            new Thread(new Runnable() {

                @Override
                public void run() {
                    serviceTianguis.getPopulares(delegate_id, new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {
                                JSONObject json = new JSONObject(response.body().string());
                                JSONArray data = json.getJSONArray("data");

                                JSONObject record;
                                Pictures picture;
                                ArrayList<Pictures> timelineList = new ArrayList<>();
                                for(int i = 0; i < data.length(); i++ ){
                                    record = data.getJSONObject(i);

                                    picture = new Pictures(record.getString("image_url"), record.getInt("likes"), record.getString("image_medium_url"));
                                    picture.setId(record.getString("id"));
                                    picture.setLiked(record.getInt("liked")>0);

                                    timelineList.add(picture);
                                }

                                timelineAdapter.clear();
                                timelineAdapter.addAll(timelineList);

                                findViewById(R.id.button_populares).setAlpha(0.3F);
                                showPopular = false;
                            } catch (JSONException | IOException e) {}
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                        }
                    });
                }

            }).start();
        }else{
            timelineList = parseData("http://movil-projects.com/tt2017/api/gallery?delegate_id="+delegate_id);

            timelineAdapter.clear();
            timelineAdapter.addAll((List<Pictures>) timelineList.clone());


            findViewById(R.id.button_populares).setAlpha(1);
            showPopular = true;
        }

    }
}
