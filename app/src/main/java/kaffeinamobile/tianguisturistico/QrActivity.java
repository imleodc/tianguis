package kaffeinamobile.tianguisturistico;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.Result;

import kaffeinamobile.tianguisturistico.models.Contact;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class QrActivity extends BaseActivity implements ZXingScannerView.ResultHandler{

    private ZXingScannerView mScannerView;
    private ViewGroup contentFrame;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        setToolbar();
        activateUpButton();

        contentFrame = (ViewGroup) findViewById(R.id.scanner_view);

        mScannerView = new ZXingScannerView(this);
        contentFrame.addView(mScannerView);
    }

    @Override
    public void handleResult(Result result) {
        String scanResult = result.getText();
        String[] split = scanResult.split("\\|");

        String delegate_id = split[0];

        Contact contact = TianguisApp.findContactById(delegate_id);

        if(contact != null){
            Intent intent = new Intent(getApplicationContext(), ContactActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("contact", contact);

            getApplicationContext().startActivity(intent);
        }else{
            contentFrame.removeAllViews();

            mScannerView = new ZXingScannerView(this);
            contentFrame.addView(mScannerView);

            Toast.makeText(getApplicationContext(), getString(R.string.error_qr), Toast.LENGTH_SHORT).show();
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

}
