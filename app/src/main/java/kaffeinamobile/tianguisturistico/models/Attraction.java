package kaffeinamobile.tianguisturistico.models;

import java.io.Serializable;


public class Attraction implements Serializable{

    private String id;
    private String categoryId;

    private String name;
    private String zone;
    private String address;
    private String description;
    private String contact;
    private String url;

    private int smallIcon;
    private int picture;

    private int parentPicture;

    public void setParentPicture(int parentPicture) {
        this.parentPicture = parentPicture;
    }

    public int getParentPicture() {
        return parentPicture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    public int getPicture() {
        return picture;
    }

    public void setSmallIcon(int smallIcon) {
        this.smallIcon = smallIcon;
    }

    public int getSmallIcon() {
        return smallIcon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
