package kaffeinamobile.tianguisturistico.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by lcastaneda005 on 27/02/2017.
 */

public class Contact implements Serializable {
    private String name;

    private String id;
    private String address;
    private String address2;
    private String country;
    private String phone;
    private String fax;
    private String email;
    private String url;

    private int delegates_length;
    private ArrayList<ContactDelegate> delegates = new ArrayList<>();
    private String type;

    private String company_id;
    private String title;

    private String group;

    private String member_id;
    private String delegate_id;
    private String company;

    public void setCompany(String company) {
        this.company = company;
    }

    public void setDelegate_id(String delegate_id) {
        this.delegate_id = delegate_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public String getDelegate_id() {
        return delegate_id;
    }

    public String getCompany() {
        return company;
    }

    private boolean firstToShow = false;

    public void setFirstToShow(boolean firstToShow) {
        this.firstToShow = firstToShow;
    }

     public boolean getFirstToShow(){
         return firstToShow;
     }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public Contact(){

    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany_id() {
        return company_id;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public void setDelegates_length(int delegates_length) {
        this.delegates_length = delegates_length;
    }

    public int getDelegates_length() {
        return delegates_length;
    }

    public void addDelegate(String id, String company_id, String name, String title, String email){
        delegates.add(new ContactDelegate(id, company_id, name, title, email));
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        Contact contact = (Contact) obj;
        return contact.delegate_id.equals(delegate_id);
    }
}
