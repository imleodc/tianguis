package kaffeinamobile.tianguisturistico.models;

/**
 * Created by lcastaneda005 on 14/03/2017.
 */

public class ContactDelegate {
    private String id;
    private String company_id;
    private String name;
    private String title;
    private String email;

    public ContactDelegate(String id, String company_id, String name, String title, String email) {
        this.id = id;
        this.company_id = company_id;
        this.name = name;
        this.title = title;
        this.email = email;
    }

    public ContactDelegate(){}


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
