package kaffeinamobile.tianguisturistico.models;

import java.io.Serializable;

/**
 * Created by lcastaneda005 on 15/03/2017.
 */

public class Meeting implements Serializable{

    private String day;
    private String time;
    private String name;
    private String group;

    private String slotId;
    private String slotNumber;

    private String contact_id;

    private boolean filled = false;

    private String registrationType;
    private String appointmentDelegate_id;
    private String appointmentCode;

    public Meeting(String registrationType) {
        this.registrationType = registrationType;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public boolean getFilled(){
        return filled;
    }


    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getSlotId() {
        return slotId;
    }

    public String getSlotNumber() {
        return slotNumber;
    }

    public void setSlotNumber(String slotNumber) {
        this.slotNumber = slotNumber;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void validate(){
        filled = !(name == null || name.isEmpty());
    }

    public boolean isBuyer() {
        return registrationType.equals("Buyer");
    }

    public String getAppointmentDelegate_id() {
        return appointmentDelegate_id;
    }

    public void setAppointmentDelegate_id(String appointmentDelegate_id) {
        this.appointmentDelegate_id = appointmentDelegate_id;
    }

    public void setAppointmentCode(String appointmentCode) {
        this.appointmentCode = appointmentCode;
    }

    public String getAppointmentCode() {
        return appointmentCode;
    }
}
