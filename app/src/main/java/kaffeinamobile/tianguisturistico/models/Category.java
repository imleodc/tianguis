package kaffeinamobile.tianguisturistico.models;

import java.io.Serializable;

public class Category implements Serializable{

    private String id = "";
    private String name = "";
    private String image = "";

    private int smallIcon_id;
    private int background_id;

    public Category(){
    }

    public void setBackground_id(int background_id) {
        this.background_id = background_id;
    }

    public void setSmallIcon_id(int smallIcon_id) {
        this.smallIcon_id = smallIcon_id;
    }

    public int getBackground_id() {
        return background_id;
    }

    public int getSmallIcon_id() {
        return smallIcon_id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }
}
