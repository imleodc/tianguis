package kaffeinamobile.tianguisturistico.models;

/**
 * Created by lcastaneda005 on 12/03/2017.
 */

public class Premios {

    private String text;
    private String score;
    private boolean completed;


    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getScore() {
        return score;
    }

    public String getText() {
        return text;
    }

    public boolean getCompleted(){
        return completed;
    }
}
