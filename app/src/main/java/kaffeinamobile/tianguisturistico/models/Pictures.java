package kaffeinamobile.tianguisturistico.models;

import java.io.Serializable;

/**
 * Created by lcastaneda005 on 11/03/2017.
 */

public class Pictures {

    private String url;
    private int likes;
    private String image_thumb_url;

    private String id;

    private String delegate_id;

    public void setDelegate_id(String delegate_id) {
        this.delegate_id = delegate_id;
    }

    public String getDelegate_id() {
        return delegate_id;
    }

    private boolean liked;

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean getLiked() {
        return liked;
    }

    public String getImage_thumb_url() {
        return image_thumb_url;
    }

    public int getLikes() {
        return likes;
    }

    public String getUrl() {
        return url;
    }

    public Pictures(String url, int likes, String image_thumb_url){
        this.url = url;
        this.likes = likes;
        this.image_thumb_url = image_thumb_url;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void liked() {
        likes++;
    }
}
