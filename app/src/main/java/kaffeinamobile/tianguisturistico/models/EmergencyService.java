package kaffeinamobile.tianguisturistico.models;

/**
 * Created by lcastaneda005 on 14/03/2017.
 */

public class EmergencyService {
    private String name;
    private String description;
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
