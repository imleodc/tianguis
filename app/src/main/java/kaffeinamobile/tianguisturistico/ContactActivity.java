package kaffeinamobile.tianguisturistico;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kaffeinamobile.tianguisturistico.models.Contact;

public class ContactActivity extends BaseActivity {

    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        setToolbar();
        activateUpButton();

        Intent i = getIntent();

        contact = (Contact) i.getSerializableExtra("contact");

        ((TextView) findViewById(R.id.contact_name)).setText(contact.getName());
        ((TextView) findViewById(R.id.contact_group)).setText(contact.getGroup());
        //((TextView) findViewById(R.id.contact_desc)).setText(contact.getAddress());

        if(isValid(contact.getUrl())) {
            ((TextView) findViewById(R.id.contact_url)).setText(contact.getUrl());
        }else{
            findViewById(R.id.contact_url).setVisibility(View.GONE);
        }

        if(isValid(contact.getPhone())) {
            ((TextView) findViewById(R.id.contact_tel)).setText(contact.getPhone());
        }else{
            findViewById(R.id.contact_tel).setVisibility(View.GONE);
        }
    }

    private boolean isValid(String text){
        return (text!=null && !text.isEmpty() && !text.equals("null"));
    }

    public void addToFav(View view) {
        if(((TianguisApp)getApplication()).addContactToFav(contact)){
            Intent favIntent = new Intent(getApplicationContext(), ContactToFav.class);
            favIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            favIntent.putExtra("name", contact.getName());

            getApplicationContext().startActivity(favIntent);
        }else{
            Toast.makeText(getApplicationContext(), getString(R.string.contact_already_fav), Toast.LENGTH_SHORT).show();
        }
    }

    public void donwloadContact(View view) {
        Intent newContactIntent = new Intent(Intent.ACTION_INSERT);
        newContactIntent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        newContactIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        newContactIntent.putExtra(ContactsContract.Intents.Insert.NAME, contact.getName());
        newContactIntent.putExtra(ContactsContract.Intents.Insert.PHONE, contact.getPhone());
        newContactIntent.putExtra(ContactsContract.Intents.Insert.COMPANY, contact.getGroup());

        getApplicationContext().startActivity(newContactIntent);
    }

    public void goToUrl(View view) {
        String contactUrl = contact.getUrl();
        String url = (!contactUrl.startsWith("http://") && !contactUrl.startsWith("https://")) ? "http://" + contactUrl:contactUrl;
        startIntent(Intent.ACTION_VIEW, url);
    }

    public void requestCall(View view) {
        String data = contact.getPhone();

        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(data);

        Intent newIntent;
        if (m.matches()) {
            String mailto = "mailto:" + data;

            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse(mailto));

            try {
                startActivity(emailIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(), getString(R.string.error_no_mail_intent), Toast.LENGTH_SHORT).show();
            }

        }else{
            newIntent = new Intent(Intent.ACTION_DIAL);

            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            newIntent.setData(Uri.parse("tel:" + data));

            getApplicationContext().startActivity(newIntent);
        }
    }

    private void startIntent(String intentId, String data){
        Intent newIntent = new Intent(intentId);

        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.setData(Uri.parse(data));

        getApplicationContext().startActivity(newIntent);
    }

}
