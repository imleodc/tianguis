package kaffeinamobile.tianguisturistico;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.models.Sponsor;

public class SponsorsActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);

        setToolbar();

        /*
        ArrayList<Sponsor> sponsors = ((TianguisApp) getApplication()).getSponsors();

        GridLayout layout = (GridLayout) findViewById(R.id.sponsors_layout);
        Sponsor sponsor;
        for(int i = 0; i < sponsors.size(); i++){
            sponsor = sponsors.get(i);
            ImageView imageView = buildImage(sponsor, i);
            layout.addView(imageView);
        }
        */
    }


    private ImageView buildImage(Sponsor sponsor){
        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        param.height = GridLayout.LayoutParams.WRAP_CONTENT;
        param.width = GridLayout.LayoutParams.WRAP_CONTENT;
        param.rightMargin = 5;
        param.topMargin = 5;
        param.setGravity(Gravity.CENTER);
        param.columnSpec = GridLayout.spec(sponsor.getPriority());
        param.rowSpec = GridLayout.spec(1);

        ImageView oImageView = new ImageView(this);
        oImageView.setImageResource(sponsor.getImage());
        oImageView.setLayoutParams (param);

        return oImageView;
    }
}
