package kaffeinamobile.tianguisturistico;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by lcastaneda005 on 24/03/2017.
 */

public class SuccessfulAppointment extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_success);

        setToolbar();
        activateUpButton();
    }
}
