package kaffeinamobile.tianguisturistico;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;

import java.util.List;

import kaffeinamobile.tianguisturistico.adapters.DirectoryContentAdapter;
import kaffeinamobile.tianguisturistico.models.Contact;

public class FavActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav);

        setToolbar();
        activateUpButton();

        List<Contact> favList = ((TianguisApp) getApplication()).getContactsFav();
        DirectoryContentAdapter adapter = new DirectoryContentAdapter(this, favList, true);

        ListView listView = (ListView) findViewById(R.id.listview_fav);
        listView.setAdapter(adapter);
    }
}
