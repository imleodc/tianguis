package kaffeinamobile.tianguisturistico;


import android.Manifest;
import android.app.Activity;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;

import kaffeinamobile.tianguisturistico.net.HttpGetRequest;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TakePictureActvity extends BaseActivity implements LoaderManager.LoaderCallbacks<Object> {

    private final int LOCAL = 1;
    private final int TAKED = 2;

    private final int PERMITS_EXTERNAL_STORAGE = 111;
    private final int PERMITS_CAMERA = 112;

    private String customFileName;

    private ServiceTianguis serviceTiaguis = new ServiceTiaguisImp();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_picture);

        Toolbar myChildToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);

        activateUpButton();
    }

    private String getRandomName(){
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

    private void startFileChooser(){
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        startActivityForResult(pickIntent, LOCAL);
    }


    public void uploadLocal(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMITS_EXTERNAL_STORAGE);
        } else {
            startFileChooser();
        }
    }

    public void takePicture(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMITS_CAMERA);
        } else {
            startCamera();
        }
    }

    private void startCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        Uri tempFile = configTempFile();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempFile);

        startActivityForResult(intent, TAKED);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File photoFile;
        File imagePath;

        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case LOCAL:
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    photoFile = new File(picturePath);
                    postPicture(photoFile);
                    break;
                case TAKED:
                    imagePath = new File(getApplicationContext().getFilesDir(), "pictures");
                    photoFile = new File(imagePath,  customFileName);

                    postPicture(photoFile);
                    break;
            }
        }
    }

    protected Uri configTempFile() {
        customFileName = getRandomName() + ".jpg";

        File imagePath = new File(getApplicationContext().getFilesDir(), "pictures");
        if(!imagePath.exists()){
            imagePath.mkdirs();
        }

        File newFile = new File(imagePath, customFileName);
        if(!newFile.exists()){
            try {
                newFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return FileProvider.getUriForFile(getApplicationContext(), "kaffeinamobile.tianguisturistico.fileprovider", newFile);
    }

    private void postPicture(final File photoFile){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", getString(R.string.posting_picture), true, false);
        ringProgressDialog.show();

        SharedPreferences settings = getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, MODE_PRIVATE);

        serviceTiaguis.sharePhoto(settings.getString("delegate_id", ""), "", photoFile, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                getLoaderManager().destroyLoader(TAKED);
                ringProgressDialog.dismiss();

                int textAux = (response.isSuccessful()) ? R.string.sucess_post_picture:R.string.error_posting_picture;
                Toast.makeText(getApplicationContext(), textAux, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ringProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), R.string.error_posting_picture, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case PERMITS_EXTERNAL_STORAGE:
                    startFileChooser();
                    break;
                case PERMITS_CAMERA:
                    startCamera();
                    break;
            }
        }
    }


    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {

    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }
}
