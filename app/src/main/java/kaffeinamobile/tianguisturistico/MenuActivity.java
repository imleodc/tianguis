package kaffeinamobile.tianguisturistico;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kaffeinamobile.tianguisturistico.services.StepDetectorService;

public class MenuActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu);

        startService(new Intent(this, StepDetectorService.class));
    }


    public void startDirectoryActivity(View view){
        Intent intent = new Intent(this, DirectoryActivity.class);
        startActivity(intent);
    }

    public void startSitiosActivity(View view){
        Intent intent = new Intent(this, AttractionsCategoryActivity.class);
        startActivity(intent);
    }

     public void startTimeLineActivity(View view){
         Intent intent = new Intent(this, TimeLineLoginActivity.class);
         startActivity(intent);
     }

     public void startServiciosEmergenciaActivity(View view){
         Intent intent = new Intent(this, ServiciosEmergenciaActivity.class);
         startActivity(intent);
     }


    public void startPremiosActivity(View view){
        Intent intent = new Intent(this, PremiosLoginActivity.class);
        startActivity(intent);
    }


    public void startRutasActivity(View view) {
        Intent intent = new Intent(this, RutasActivity.class);
        startActivity(intent);
    }

    public void startMapasActivity(View view) {
        Intent intent = new Intent(this, MapasActivity.class);
        startActivity(intent);
    }

    public void startAgendaActivity(View view) {
        Intent intent = new Intent(this, AgendaLoginActivity.class);
        startActivity(intent);
    }

    public void startSponsorsActivity(View view) {
        Intent intent = new Intent(this, SponsorsActivity.class);
        startActivity(intent);
    }
}
