package kaffeinamobile.tianguisturistico;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

public class ContactToFav extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_added_fav);

        setToolbar();
        activateUpButton();

        String name = getIntent().getStringExtra("name");

        ((TextView)findViewById(R.id.contact_fav_name)).setText(name);
    }

    public void startFav(View view) {
        Intent intent = new Intent(getApplicationContext(), FavActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
    }
}
