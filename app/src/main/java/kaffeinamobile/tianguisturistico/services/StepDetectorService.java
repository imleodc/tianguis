package kaffeinamobile.tianguisturistico.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import kaffeinamobile.tianguisturistico.TianguisApp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StepDetectorService extends Service implements SensorEventListener{

    private final int limit = 5;

    @Override
    public void onCreate() {
        super.onCreate();

        SensorManager mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor mSensor = (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR, true)!=null) ? mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR, true):mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        if(mSensor!=null){
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType()==Sensor.TYPE_STEP_DETECTOR){
            updateCounter();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private synchronized void updateCounter(){
        SharedPreferences settings = getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, MODE_PRIVATE);
        int step_counter = settings.getInt("step_counter", 0);

        step_counter++;

        SharedPreferences.Editor edit = settings.edit();
        edit.putInt("step_counter", step_counter);
        edit.apply();
    }

}
