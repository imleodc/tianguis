package kaffeinamobile.tianguisturistico;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import kaffeinamobile.tianguisturistico.models.Premios;
import kaffeinamobile.tianguisturistico.net.services.ServiceTiaguisImp;
import kaffeinamobile.tianguisturistico.net.services.ServiceTianguis;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PremiosLoginActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premios_login);

        setToolbar();

        final SharedPreferences settings = getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, MODE_PRIVATE);
        String delegate_id = settings.getString("delegate_id", "");
        final int step_counter = settings.getInt("step_counter", 0);
        final int limit = 100;

        EditText viewById = (EditText) findViewById(R.id.user_id);
        viewById.setText(settings.getString("delegate_id", ""));

        if(step_counter>limit){
            ServiceTianguis serviceTiaguis = new ServiceTiaguisImp();

            int points = step_counter/limit;
            final int auxStep = step_counter - (points*limit);

            serviceTiaguis.addPoint(delegate_id, String.valueOf(points), new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        SharedPreferences.Editor edit = settings.edit();
                        edit.putInt("step_counter", auxStep);
                        edit.commit();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {}
            });
        }
    }

    public void ingresar(View view) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(this, "", getString(R.string.loading), true, false);
        ringProgressDialog.show();

        EditText userInput = (EditText) findViewById(R.id.user_id);

        final String delegate_id = userInput.getText().toString();
        ServiceTianguis serviceTianguis = new ServiceTiaguisImp();

        serviceTianguis.getPoints(delegate_id, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ringProgressDialog.dismiss();
                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        JSONObject json = new JSONObject(result);

                        if(json.getBoolean("status")){
                            SharedPreferences settings = getSharedPreferences(TianguisApp.TIANGUIS_APP_MEMORY, MODE_PRIVATE);
                            SharedPreferences.Editor edit = settings.edit();
                            edit.putString("delegate_id", delegate_id);
                            edit.apply();

                            Intent intent = new Intent(getApplicationContext(), PremiosActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            JSONObject data = json.getJSONObject("data");
                            intent.putExtra("user_steps", data.getString("steps"));
                            intent.putExtra("user_photos", data.getString("photos"));
                            intent.putExtra("user_appointments", data.getString("appointments"));

                            getApplicationContext().startActivity(intent);
                        }else {
                            String errors = json.getJSONObject("errors").getString("delegate_id");
                            Toast.makeText(getApplicationContext(), errors, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        Toast.makeText(getApplicationContext(), getString(R.string.general_error), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), getString(R.string.general_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ringProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), getString(R.string.error_login), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
