package kaffeinamobile.tianguisturistico;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.adapters.AgendaAdapter;
import kaffeinamobile.tianguisturistico.models.Meeting;


public class AgendaActivity extends BaseActivity{

    //private ArrayList<Meeting> meeting_27 = new ArrayList<>();
    private ArrayList<Meeting> meeting_28 = new ArrayList<>();
    private ArrayList<Meeting> meeting_29 = new ArrayList<>();
    private ArrayList<Meeting> meeting_30 = new ArrayList<>();

    private String registrationType;

    private ListView listAgenda;

    private ArrayList<TextView> buttons = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_agenda);

        //buttons.add((TextView) findViewById(R.id.agenda_button_27));
        buttons.add((TextView) findViewById(R.id.agenda_button_28));
        buttons.add((TextView) findViewById(R.id.agenda_button_29));
        buttons.add((TextView) findViewById(R.id.agenda_button_30));

        setToolbar();
        activateUpButton();

        String result = getIntent().getStringExtra("user_data");
        JSONObject json = null;
        try {
            json = new JSONObject(result);
            JSONObject data = json.getJSONObject("data");
            JSONObject dataAttributes = data.getJSONObject("@attributes");
            JSONObject slots = data.getJSONObject("Slots");

            JSONObject majorSlots = slots.getJSONObject("Major");
            JSONArray minorMajorSlots = majorSlots.getJSONArray("Minor");
            JSONObject schedule;
            JSONArray slotsMinorList;
            JSONObject slotsMinor;
            JSONObject slotAttributes;
            JSONObject slotAppointments;
            JSONObject appointmentsAttributes;

            registrationType = dataAttributes.getString("RegistrationType");

            Meeting meeting;
            for(int i = 0; i < minorMajorSlots.length(); i++){
                schedule = minorMajorSlots.getJSONObject(i);

                slotsMinorList = schedule.getJSONArray("Slot");
                for(int j = 0; j < slotsMinorList.length(); j++){
                    slotsMinor = slotsMinorList.getJSONObject(j);

                    slotAttributes = slotsMinor.getJSONObject("@attributes");
                    slotAppointments = slotsMinor.getJSONObject("Appointments").getJSONObject("0").getJSONObject("@attributes");

                    meeting = new Meeting(registrationType);
                    meeting.setTime(slotAttributes.getString("StartTime"));
                    meeting.setName(slotAppointments.getString("Target_Del"));
                    meeting.setGroup(slotAppointments.getString("Target_Cmp"));
                    meeting.setAppointmentDelegate_id(slotAppointments.getString("Target_DelNum"));
                    meeting.setSlotId(slotAppointments.getString("SlotTypeID"));
                    meeting.setSlotNumber(slotAttributes.getString("SlotNumber"));

                    meeting.validate();


                    if( i < 2 ){
                        meeting_28.add(meeting);
                    }else if(i < 4 ){
                        meeting_29.add(meeting);
                    }else{
                        meeting_30.add(meeting);
                    }
                }
            }

            AgendaAdapter adapter = new AgendaAdapter(this, meeting_28);

            listAgenda = (ListView) findViewById(R.id.listview_agenda);
            listAgenda.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void cleanButtons(TextView view){
        for(TextView button: buttons){
            button.setTextColor(Color.DKGRAY);
            button.setBackgroundResource(R.drawable.circle_empty);
        }

        view.setBackgroundResource(R.drawable.circle_filled);
        view.setTextColor(Color.WHITE);
    }

    public void show_30(View view) {
        cleanButtons((TextView) view);

        AgendaAdapter adapter = new AgendaAdapter(this, meeting_30);
        listAgenda.setAdapter(adapter);
    }

    public void show_29(View view) {
        cleanButtons((TextView) view);
        AgendaAdapter adapter = new AgendaAdapter(this, meeting_29);
        listAgenda.setAdapter(adapter);
    }

    public void show_28(View view) {
        cleanButtons((TextView) view);
        AgendaAdapter adapter = new AgendaAdapter(this, meeting_28);
        listAgenda.setAdapter(adapter);
    }

    /*
    public void show_27(View view) {
        cleanButtons((TextView) view);
        AgendaAdapter adapter = new AgendaAdapter(this, meeting_27);
        listAgenda.setAdapter(adapter);
    }
    */
}
