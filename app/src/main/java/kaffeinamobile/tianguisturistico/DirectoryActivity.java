package kaffeinamobile.tianguisturistico;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import kaffeinamobile.tianguisturistico.adapters.DirectoryContentAdapter;
import kaffeinamobile.tianguisturistico.models.Contact;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class DirectoryActivity extends BaseActivity {

    private final int SCAN_QR = 100;

    private ArrayList<Contact> contacts;


    public void paintList(){
        ListView list = (ListView) findViewById(R.id.listview);

        final DirectoryContentAdapter adapter = new DirectoryContentAdapter(this, contacts);

        list.setAdapter(adapter);
        final EditText editsearch = (EditText) findViewById(R.id.search);

        // Capture Text in EditText
        editsearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);

        setToolbar();


        contacts = (ArrayList<Contact>) TianguisApp.contacts.clone();
        String stringExtra = getIntent().getStringExtra("show_group");

        System.out.println("------------------------------------------"+stringExtra);
        if(stringExtra!=null && !stringExtra.isEmpty()){
            //GROUP = stringExtra.toUpperCase();
            ArrayList<Contact> contactsAux  = new ArrayList<>();

            Contact contact;
            for(int i = 0; i < contacts.size(); i++){
                contact = contacts.get(i);
                if(stringExtra.equals(contact.getGroup())){
                    contactsAux.add(contact);
                }
            }

            contacts = contactsAux;
        }

        paintList();
    }

    public void startFav(View view) {
        Intent intent = new Intent(getApplicationContext(), FavActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void readQR(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, SCAN_QR);
        } else {
            startScanner();
        }
    }

    private void startScanner(){
        Intent intent = new Intent(getApplicationContext(), QrActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case SCAN_QR:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startScanner();
                }
            break;
        }
    }

}
