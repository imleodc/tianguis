package kaffeinamobile.tianguisturistico.net.services;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Callback;

public interface ServiceTianguis {

    void sharePhoto(String delegate_id, String comment, File  photo, Callback<ResponseBody> callback);

    void likePhoto(String email, String post_id, Callback<ResponseBody> callback);

    void getPopulares(String delegate_id, Callback<ResponseBody> callback);

    void getPoints(String delegate_id, Callback<ResponseBody> callback);

    void addPoint(String delegate_id, String km, Callback<ResponseBody> callback);

    void getTimeline(String delegate_id, Callback<ResponseBody> callback);

    void getAgenda(String delegate_id, Callback<ResponseBody> callback);

    void getAppointment(String delegate_1_id, String delegate_2_id, String slot_number, Callback<ResponseBody> callback);

    void confirmAppointment(String code, String delegate_2_id, String slot_number, Callback<ResponseBody> callback);
}
