package kaffeinamobile.tianguisturistico.net.services;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import kaffeinamobile.tianguisturistico.net.APITianguisRetrofit;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceTiaguisImp implements ServiceTianguis{

    private final String SERVICE_HOME = "http://movil-projects.com/tt2017/";

    private Retrofit retrofit;

    public ServiceTiaguisImp(){
        retrofit = new Retrofit.Builder()
                .baseUrl(SERVICE_HOME)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Override
    public void sharePhoto(String delegate_id, String comment, File photo, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        RequestBody delegateRB = RequestBody.create(MediaType.parse("text/plain"), delegate_id);
        RequestBody commentRB = RequestBody.create(MediaType.parse("text/plain"), comment);

        Map<String, RequestBody> parameters = new HashMap<>();
        parameters.put("delegate_id", delegateRB);
        parameters.put("comment", commentRB);

        MultipartBody.Part photoPart = prepareFilePart("image", photo);
        Call<ResponseBody> call = api.sharePhoto(parameters, photoPart);

        call.enqueue(callback);
    }

    @Override
    public void likePhoto(String email, String post_id, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.likePhoto(email, post_id);
        call.enqueue(callback);
    }



    @Override
    public void getPopulares(String delegate_id, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.getPopulares(delegate_id);
        call.enqueue(callback);
    }

    @Override
    public void getPoints(String delegate_id, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.getPoints(delegate_id);
        call.enqueue(callback);
    }

    @Override
    public void addPoint(String delegate_id, String km, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.addPoint(delegate_id, km);
        call.enqueue(callback);
    }

    @Override
    public void getTimeline(String delegate_id, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.getTimeline(delegate_id);
        call.enqueue(callback);
    }

    @Override
    public void getAgenda(String delegate_id, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.getAgenda(delegate_id);
        call.enqueue(callback);
    }

    @Override
    public void getAppointment(String delegate_1_id, String delegate_2_id, String slot_number, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.getAppointment(delegate_1_id, delegate_2_id, slot_number);
        call.enqueue(callback);
    }

    @Override
    public void confirmAppointment(String code, String delegate_2_id, String slot, Callback<ResponseBody> callback) {
        APITianguisRetrofit api = retrofit.create(APITianguisRetrofit.class);

        Call<ResponseBody> call = api.confirmAppointment(code, delegate_2_id, slot);
        call.enqueue(callback);
    }

    private MultipartBody.Part prepareFilePart(String partName, File photo){
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), photo);
        return MultipartBody.Part.createFormData(partName, "photo.jpg", requestBody);
    }


}
