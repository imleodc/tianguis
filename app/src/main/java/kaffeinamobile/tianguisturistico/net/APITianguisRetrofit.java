package kaffeinamobile.tianguisturistico.net;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface APITianguisRetrofit {

    @Multipart
    @POST("api/post")
    Call<ResponseBody> sharePhoto(@PartMap Map<String, RequestBody> parameters, @Part MultipartBody.Part file);

    @GET("api/like")
    Call<ResponseBody> likePhoto(@Query("delegate_id") String delegate_id, @Query("post_id") String post_id);

    @POST("api/popular")
    Call<ResponseBody> getPopulares(@Query("delegate_id") String delegate_id);

    @POST("api/points-list")
    Call<ResponseBody> getPoints(@Query("delegate_id") String delegate_id);

    @POST("api/steps")
    Call<ResponseBody> addPoint(@Query("delegate_id") String delegate_id, @Query("km") String km);

    @GET("api/gallery")
    Call<ResponseBody> getTimeline(@Query("delegate_id") String delegate_id);

    @GET("api/agenda/{delegate_id}")
    Call<ResponseBody> getAgenda(@Path("delegate_id") String delegate_id);

    @POST("api/appointment")
    Call<ResponseBody> getAppointment(@Query("delegate_1_id") String delegate_1_id, @Query("delegate_2_id") String delegate_2_id, @Query("slot_number") String slot_number);

    @POST("api/confirm-appointment")
    Call<ResponseBody> confirmAppointment(@Query("code") String code, @Query("delegate_2_id") String delegate_2_id, @Query("slot_number") String slot_number);

}
