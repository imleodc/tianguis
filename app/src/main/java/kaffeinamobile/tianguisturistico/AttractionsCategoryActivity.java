package kaffeinamobile.tianguisturistico;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kaffeinamobile.tianguisturistico.adapters.AttractionsCategoryAdapter;
import kaffeinamobile.tianguisturistico.models.Category;

public class AttractionsCategoryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attractions_category);

        setToolbar();

        ArrayList<Category> data = getData();
        AttractionsCategoryAdapter categoryAdapter = new AttractionsCategoryAdapter(this, data);

        ListView list = (ListView) findViewById(R.id.category_listview);
        list.setAdapter(categoryAdapter);
    }


    private ArrayList<Category> getData(){

        ArrayList<Category> categories = new ArrayList<>();
        String req = "{\"status\":true,\"data\":[{\"id\":\"1\",\"name\":\"Restaurantes\",\"created_at\":\"2017-02-27 13:32:22\",\"updated_at\":\"2017-02-27 13:32:22\",\"image_file_name\":null,\"image_file_size\":null,\"image_content_type\":null,\"image_updated_at\":null},{\"id\":\"2\",\"name\":\"Hoteles\",\"created_at\":\"2017-02-27 13:32:22\",\"updated_at\":\"2017-02-27 13:32:22\",\"image_file_name\":null,\"image_file_size\":null,\"image_content_type\":null,\"image_updated_at\":null},{\"id\":\"3\",\"name\":\"Discotecas y Bares\",\"created_at\":\"2017-02-27 13:32:22\",\"updated_at\":\"2017-02-27 13:32:22\",\"image_file_name\":null,\"image_file_size\":null,\"image_content_type\":null,\"image_updated_at\":null},{\"id\":\"4\",\"name\":\"Buceo\",\"created_at\":\"2017-02-27 13:32:22\",\"updated_at\":\"2017-02-27 13:32:22\",\"image_file_name\":null,\"image_file_size\":null,\"image_content_type\":null,\"image_updated_at\":null},{\"id\":\"5\",\"name\":\"Yates\",\"created_at\":\"2017-02-27 13:32:22\",\"updated_at\":\"2017-02-27 13:32:22\",\"image_file_name\":null,\"image_file_size\":null,\"image_content_type\":null,\"image_updated_at\":null},{\"id\":\"6\",\"name\":\"Hospitales\",\"created_at\":\"2017-03-06 17:52:25\",\"updated_at\":\"2017-03-06 17:52:25\",\"image_file_name\":null,\"image_file_size\":null,\"image_content_type\":null,\"image_updated_at\":null}]}";

        try {
            JSONObject json = new JSONObject(req);
            JSONArray data = json.getJSONArray("data");

            JSONObject categoryObject;
            Category category;
            for(int i = 0; i < data.length(); i++ ){
                categoryObject = data.getJSONObject(i);
                category = new Category();

                category.setName(categoryObject.getString("name"));
                category.setId(categoryObject.getString("id"));
                category.setImage(categoryObject.getString("image_file_name"));

                int smallIcon = R.drawable.icon_small_restaurantes;
                int backgroundId = R.drawable.places01;

                switch(category.getId()){
                    case "2":
                        smallIcon = R.drawable.icon_small_hoteles;
                        backgroundId = R.drawable.places02;
                        break;
                    case "3":
                        smallIcon = R.drawable.icon_small_bar;
                        backgroundId = R.drawable.places03;
                        break;
                    case "4":
                        smallIcon = R.drawable.icon_small_buceo;
                        backgroundId = R.drawable.places04;
                        break;
                    case "5":
                        smallIcon = R.drawable.icon_small_yates;
                        backgroundId = R.drawable.places05;
                        break;
                    case "6":
                        smallIcon = R.drawable.icon_small_hospital;
                        backgroundId = R.drawable.places06;
                        break;
                }

                category.setSmallIcon_id(smallIcon);
                category.setBackground_id(backgroundId);


                categories.add(category);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return categories;
    }
}
